using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Xml;
using System.Collections.Generic;

public class FBXAnimEvents : AssetPostprocessor
{
    void OnPostprocessModel(GameObject go)
    {
        m_GameObject = go;
        string filename = Path.GetDirectoryName(assetPath) + "/" + Path.GetFileNameWithoutExtension(assetPath) + ".animevents.xml";

        if (File.Exists(filename))
        {
			m_Animation = go.GetComponent<Animation>();

			if (!m_Animation)
			{
				Debug.LogError("Found animevents.xml file, but model doesn't have Animation component. Make sure the Animation type is set to Legacy " + filename);
				return;
			}

            m_Filename = filename;
            ProcessAnimEventsFile();
            m_GameObject.AddComponent(typeof(AnimationEventHandler));
        }
    }

    void ProcessAnimEventsFile()
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(m_Filename);
        XmlElement root = doc.DocumentElement;

        if (root == null || root.Name != "Animations")
        {
            Debug.LogError(m_Filename + ": Expected node 'Animations' as root");
            return;
        }

        foreach (XmlNode animNode in root.ChildNodes)
        {
            XmlElement animElement = animNode as XmlElement;

            if (animElement != null)
            {
                if (animElement.Name == "Anim")
                {
                    string animName = animElement.GetAttribute("name");
                    AnimationClip animation = m_Animation.GetClip(animName);

                    if (animation)
                    {
                        List<AnimationEvent> events = ProcessAnimation(animElement, animation);
                        AnimationUtility.SetAnimationEvents(animation, events.ToArray());
                    }
                    else
                    {
                        Debug.LogWarning(m_Filename + ": Can't find animation clip " + animName);
                    }
                }
                else
                {
                    Debug.LogError(m_Filename + ": Unexpected node: " + animElement.Name);
                }
            }
        }
    }

    List<AnimationEvent> ProcessAnimation(XmlElement animationElement, AnimationClip animation)
    {
        List<AnimationEvent> events = new List<AnimationEvent>();

        foreach (XmlNode eventNode in animationElement.ChildNodes)
        {
            XmlElement eventElement = eventNode as XmlElement;

            if (eventElement != null)
            {
                int frame = 0;
                if (!int.TryParse(eventElement.GetAttribute("frame"), out frame))
                {
                    Debug.LogError(m_Filename + ": " + animation.name + ": Expected 'frame' attribute");
                    continue;
                }

                string function = eventElement.GetAttribute("function");
                if (function.Length == 0)
                {
                    Debug.LogError(m_Filename + ": " + animation.name + ": Expected 'function' attribute");
                    continue;
                }

                AnimationEvent animEvent = new AnimationEvent();
                animEvent.time = (float)frame / animation.frameRate;
                animEvent.functionName = "OnAnimationEvent";
                animEvent.stringParameter = function;
                animEvent.messageOptions = SendMessageOptions.DontRequireReceiver;
                events.Add(animEvent);
            }
        }

        return events;
    }

    GameObject m_GameObject;
	Animation m_Animation;
    string m_Filename;
}
