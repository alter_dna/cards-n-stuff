﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Agent : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/

	[Header("Agent Settings")]
	public Point2 Footprint = Point2.one;

	// Speed the agent walks at, in meters per second
	public float Speed = 1.0f;

	/************************************************************************/
	/* PUBLIC PROPERTIES                                                    */
	/************************************************************************/

	public Grid ParentGrid { get { return m_ParentGrid; } }
	public Player OwnerPlayer { get { return m_OwnerPlayer; } }
	public bool IsMoving { get { return m_Moving; } }

	/************************************************************************/
	/* PUBLIC METHODS                                                       */
	/************************************************************************/

	public void Init(Grid grid, Player player, Vector2 location)
	{
		m_OwnerPlayer = player;
		m_ParentGrid = grid;
		transform.position = (grid.transform.right * location.x) + (grid.transform.forward * location.y);
		transform.localRotation = player.GetSpawnRotationForAgent(this);
	}

	public Rect GetFootprintRect()
	{
		return GetFootprintRect(m_ParentGrid);
	}

	public Rect GetFootprintRect(Grid grid)
	{
		float inset = 0.1f;
		Vector2 halfSize = new Vector2(Footprint.x * grid.CellSize.x * 0.5f - inset, Footprint.y * grid.CellSize.y * 0.5f - inset);

		return Rect.MinMaxRect(-halfSize.x, -halfSize.y, halfSize.x, halfSize.y);
	}

	// Gets a list of all the cells the agent is currently occupying
	public List<GridCell> GetOccupyingCells()
	{
		Vector2 position = new Vector2(transform.localPosition.x, transform.localPosition.z);

		List<GridCell> cells = GetCellsForFootprint(m_ParentGrid, position);

		// Add all the target cells as well
		foreach(GridCell cell in m_TargetCells)
		{
			if (!cells.Contains(cell))
			{
				cells.Add(cell);
			}
		}

		return cells;
	}

	// Get the cells from the grid that match the agents footprint at the given position.
	// NOTE: This CAN return null cells, which means the footprint is going off the grid.
	public List<GridCell> GetCellsForFootprint(Grid grid, Vector2 position)
	{
		List<GridCell> result = new List<GridCell>();

		Rect rect = GetFootprintRect(grid);

		int xMin = Mathf.FloorToInt(position.x + rect.xMin);
		int xMax = Mathf.CeilToInt(position.x + rect.xMax);
		int yMin = Mathf.FloorToInt(position.y + rect.yMin);
		int yMax = Mathf.CeilToInt(position.y + rect.yMax);

		for (int y = yMin; y < yMax; y++)
		{
			for (int x = xMin; x < xMax; x++)
			{
				GridCell cell = grid.GetCell(x, y);
				result.Add(cell);
			}
		}

		return result;
	}

	public Grid.PlacementStatusEnum CanPlace(Vector2 position)
	{
		return m_ParentGrid.CanPlaceAtPosition(this, position);
	}

	public void DrawDebugRect(Grid grid, Vector3 position)
	{
		Rect rect = GetFootprintRect(grid);

		Vector3 right = grid.transform.right;
		Vector3 forward = grid.transform.forward;
		Color color = Color.red;

		Debug.DrawLine(position + right * rect.xMin + forward * rect.yMin, position + right * rect.xMax + forward * rect.yMin, color);
		Debug.DrawLine(position + right * rect.xMin + forward * rect.yMax, position + right * rect.xMax + forward * rect.yMax, color);
		Debug.DrawLine(position + right * rect.xMin + forward * rect.yMin, position + right * rect.xMin + forward * rect.yMax, color);
		Debug.DrawLine(position + right * rect.xMax + forward * rect.yMin, position + right * rect.xMax + forward * rect.yMax, color);
	}

	public bool MoveCellOffset(Point2 offset, out HitResult hitResult)
	{
		Vector3 originPosition = transform.position;
		Vector3 targetPosition = transform.position + new Vector3(offset.x * m_ParentGrid.CellSize.x, 0.0f, offset.y * m_ParentGrid.CellSize.y);

		if (CanMoveTo(targetPosition, out hitResult))
		{
			m_OriginPosition = originPosition;
			m_TargetPosition = targetPosition;
			m_DistanceWalked = 0.0f;
			m_Moving = true;
			m_TargetCells = hitResult.OverlappingCells;
			return true;
		}
		else
		{
			m_TargetCells.Clear();
			return false;
		}
	}

	public bool CanMoveTo(Vector3 position, out HitResult hitResult)
	{
		return m_ParentGrid.HitTestAgent(this, new Vector2(position.x, position.z), out hitResult) == false;
	}

	public virtual void TakeDamage(Agent attacker, int damage)
	{
		// Base agents do nothing
	}

	/************************************************************************/
	/* MESSAGES                                                             */
	/************************************************************************/

	// Use this for initialization
	protected virtual void Start () 
	{
	
	}
	
	// Update is called once per frame
	protected virtual void Update() 
	{
		UpdateMovement();
		DrawDebugRect(m_ParentGrid, transform.position);
	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	void UpdateMovement()
	{
		if (m_Moving)
		{
			m_DistanceWalked += Speed * Time.deltaTime;

			float distance = Vector3.Distance(m_OriginPosition, m_TargetPosition);
			if (m_DistanceWalked >= distance)
			{
				transform.position = m_TargetPosition;
				m_Moving = false;
			}
			else
			{
				transform.position += Speed * Time.deltaTime * transform.forward;
			}
		}
	}

	/************************************************************************/
	/* PRIVATE VARIABLES                                                    */
	/************************************************************************/

	bool m_Moving;
	Vector3 m_OriginPosition;
	Vector3 m_TargetPosition;
	float m_DistanceWalked;
	List<GridCell> m_TargetCells = new List<GridCell>();

	Player m_OwnerPlayer;
	Grid m_ParentGrid;
}
