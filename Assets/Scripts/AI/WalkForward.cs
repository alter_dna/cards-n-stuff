﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WalkForward : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/

	/************************************************************************/
	/* MESSAGES                                                             */
	/************************************************************************/

	// Use this for initialization
	void Start () 
	{
		m_Agent = GetComponent<Agent>();
		m_Model = GetComponent<Model>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!m_Agent.IsMoving && m_AttackTargets.Count == 0)
		{
			Point2 offset = Point2.zero;
			offset.x = (transform.forward.x > 0) ? 1 : -1;

			HitResult hitResult = new HitResult();
			if(!m_Agent.MoveCellOffset(offset, out hitResult))
			{
				if (hitResult.HasFlag(HitResultFlags.Blocked_HitAgent))
				{
					m_AttackTargets = FindOpponentAgents(hitResult.HitAgents);
				}
				else
				{
					m_AttackTargets.Clear();
				}

				if (m_AttackTargets.Count > 0)
				{
					m_NextAnim = "attack";

					if (m_CurrentAnim == m_NextAnim)
					{
						m_CurrentAnim = null;
					}

					m_NextAnimCallback = OnAttackFinished;
				}
				else
				{
					m_NextAnim = "idle";
				}
			}
			else
			{
				m_NextAnim = "walk";
			}
		}

		UpdateAnimations();
	}

	void OnAttackFinished()
	{
		m_AttackTargets.Clear();
	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	List<Agent> FindOpponentAgents(List<Agent> listToSearch)
	{
		List<Agent> result = new List<Agent>();

		foreach (Agent agent in listToSearch)
		{
			if (agent.OwnerPlayer != m_Agent.OwnerPlayer)
			{
				result.Add(agent);
			}
		}

		return result;
	}

	void UpdateAnimations()
	{
		if (m_Model)
		{
			if (m_NextAnim != m_CurrentAnim)
			{
				if (m_CurrentAnim == null)
				{
					m_Model.PlayAnimation(m_NextAnim, m_NextAnimCallback);
				}
				else
				{
					m_Model.BlendToAnimation(m_CurrentAnim, m_NextAnim, 0.25f, m_NextAnimCallback);
				}

				m_CurrentAnim = m_NextAnim;
				m_NextAnimCallback = null;
			}
		}
	}

	/************************************************************************/
	/* PRIVATE VARIABLES                                                    */
	/************************************************************************/

	Model m_Model;
	Model.OnAnimFinished m_NextAnimCallback = null;
	string m_NextAnim = "idle";
	string m_CurrentAnim = null;
	List<Agent> m_AttackTargets = new List<Agent>();
	Agent m_Agent;
}
