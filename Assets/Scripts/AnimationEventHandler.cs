using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct AnimEventMessage
{
    public string EventName;
    public Dictionary<string, object> Arguments;
}

public class AnimationEventHandler : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnAnimationEvent(string eventName)
    {
        AnimEventMessage msg = new AnimEventMessage() { EventName = eventName, Arguments = null };

        if (transform.parent)
        {
            transform.parent.SendMessage("OnAnimationEvent", msg, SendMessageOptions.DontRequireReceiver);
        }
    }
}
