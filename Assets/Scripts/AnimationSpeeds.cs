﻿using UnityEngine;
using System.Collections;

public class AnimationSpeeds : MonoBehaviour {


	public float desiredIdleSpeed;
	public float desiredWalkSpeed;
	public float desiredAttackSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.GetComponent<Animation>() ["idle"].speed = desiredIdleSpeed;
		this.GetComponent<Animation>() ["walk"].speed = desiredWalkSpeed;
		this.GetComponent<Animation>() ["attack"].speed = desiredAttackSpeed;
	}
}
