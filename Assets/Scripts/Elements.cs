﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class Elements
{
	[Flags]
	public enum ElementType
	{
		Earth	= (1 << 0),
		Fire	= (1 << 1),
		Wind	= (1 << 2),
		Water	= (1 << 3),
		Shadow	= (1 << 4),
		Light	= (1 << 5),
		Normal	= (1 << 6),
		Taint	= (1 << 7)
	};
}
