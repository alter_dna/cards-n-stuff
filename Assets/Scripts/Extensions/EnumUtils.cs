﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class EnumUtils
{
	static public int GetAllFlagsSet<T>()
	{
		return ((1 << (System.Enum.GetValues(typeof(T)).Length + 1)) - 1);
	}
}
