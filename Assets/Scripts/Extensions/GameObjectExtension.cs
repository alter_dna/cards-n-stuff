﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class GameObjectExtension
{
	static public void SetLayerRecursively(this GameObject go, int layer)
    {
		if (go.layer == 0)
		{
			go.layer = layer;
		}

        foreach (Transform child in go.transform)
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }
}
