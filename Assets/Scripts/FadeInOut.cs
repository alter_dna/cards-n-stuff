﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour

{
	public Image myPanel;
	public float fadeTime = 3f;
	Color colorToFadeTo;


	void Start() {
		colorToFadeTo = new Color (1f, 1f, 1f, 0f);
		myPanel.CrossFadeColor (colorToFadeTo, fadeTime, true, true);
	}

}