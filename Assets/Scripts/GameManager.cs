﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/

	public Grid Grid;

	public Agent BarrierPrefab1;
	public Agent BarrierPrefab2;
	public Agent Stone1Prefab;
	public Agent Stone2Prefab;

	// Time it takes for the AI to spawn a unit
	public float AISpawnTimerMin = 4.0f;
	public float AISpawnTimerMax = 10.0f;
	
	// Max number of AI agents
	public int AIMaxAgents = 6;

	// Agents the AI picks from
	public Agent[] AIAgents;

	// Test objects to spawn on click
	public Agent[] TestAgents;

	public int randomAgent;
	public int unitNumber;

	private PlayerStats stats;




	/************************************************************************/
	/* PUBLIC PROPERTIES                                                    */
	/************************************************************************/

	static public GameManager Instance { get { return m_Instance; } }

	public Player LocalPlayer { get { return m_Players[m_LocalPlayerIndex]; } }
	public Player[] OpponentPlayers
	{ 
		get
		{
			List<Player> opponents = new List<Player>();

			for (int i = 0; i < m_Players.Count; i++)
			{
				if (i != m_LocalPlayerIndex)
				{
					opponents.Add(m_Players[i]);
				}
			}

			return opponents.ToArray();
		}
	}

	/************************************************************************/
	/* MESSAGES                                                             */
	/************************************************************************/

	void Awake()
	{
		m_Instance = this;
	}

	// Use this for initialization
	void Start () 
	{
		stats = GameObject.FindWithTag("Manager").GetComponent<PlayerStats>();
		m_PlacementTimer = AISpawnTimerMax;
		SetupPlayers();
		SetupStones ();
		randomAgent = Random.Range (0, 4);


	}
	
	// Update is called once per frame
	void Update () 
	{

		if (Grid.GetAgentCountForPlayer(OpponentPlayers[0]) < AIMaxAgents)
		{
			m_PlacementTimer -= Time.deltaTime;

			if (m_PlacementTimer <= 0.0f)
			{
				m_PlacementTimer = Random.Range(AISpawnTimerMin, AISpawnTimerMax);

				// Try to place a unit in a random position for 20 attempts, otherwise just ignore it
				for (int i = 0; i < 20; i++)
				{
					Agent agent = AIAgents[Random.Range(0, AIAgents.Length)];
					Point2 location = new Point2(Random.Range(Grid.GridSize.x / 2, Grid.GridSize.x), Random.Range(0, Grid.GridSize.y));

					if (Grid.CanPlaceAtLocation(agent, location) == Grid.PlacementStatusEnum.ValidForOpponent)
					{
						
						Grid.SpawnAgentAtLocation(agent, location);
						break;
					}
				}
			}
		}
	}

	void OnGUI()
	{
		//GUILayout.BeginVertical();
		if(stats.Mana >=1)
		{
			for (int i = 0; i < TestAgents.Length; i++)
			{
				Agent agent = TestAgents[i];
			

				//if (GUILayout.Button("" + agent.name) || (i < 9 && Input.GetKeyDown(KeyCode.Alpha1 + i)))
				if (i < 9 && Input.GetKeyDown(KeyCode.Alpha1 + i))
				{
					Grid.BeginPlacement(agent);
				}
//
//				if(GUI.Button( new Rect (100,500,200,30), "" + randomAgent))
//				{
//					Grid.BeginPlacement(TestAgents[randomAgent]);
//				}
			}
		}
		//GUILayout.EndVertical();
	}

	public void SummonCard1()
	{
		//Grid.BeginPlacement(TestAgents[randomAgent]);
		Grid.BeginPlacement(TestAgents[unitNumber]);
	}

	public void SummonCard2()
	{
		//Grid.BeginPlacement(TestAgents[randomAgent]);
		Grid.BeginPlacement(TestAgents[unitNumber+1]);
	}

	public void SummonCard3()
	{
		//Grid.BeginPlacement(TestAgents[randomAgent]);
		Grid.BeginPlacement(TestAgents[unitNumber+5]);
	}

	public void SummonCard4()
	{
		//Grid.BeginPlacement(TestAgents[randomAgent]);
		Grid.BeginPlacement(TestAgents[unitNumber+2]);
	}

	public void SummonCard5()
	{
		//Grid.BeginPlacement(TestAgents[randomAgent]);
		Grid.BeginPlacement(TestAgents[unitNumber+7]);
	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	void SetupPlayers()
	{
		m_LocalPlayerIndex = 0;

		CreatePlayer("Player1", BarrierPrefab1, new Point2(0, Mathf.FloorToInt(Grid.GridSize.y / 2.0f)));
		CreatePlayer("Player2", BarrierPrefab2, new Point2(Grid.GridSize.x - 1, Mathf.FloorToInt(Grid.GridSize.y / 2.0f)));
	}

	void SetupStones()
	{
		Grid.SpawnAgentAtLocation(Stone1Prefab, new Point2(1, 6));
		Grid.SpawnAgentAtLocation(Stone1Prefab, new Point2(1, 4));
		Grid.SpawnAgentAtLocation(Stone1Prefab, new Point2(1, 2));
		Grid.SpawnAgentAtLocation(Stone1Prefab, new Point2(1, 0));

		Grid.SpawnAgentAtLocation(Stone2Prefab, new Point2(Grid.GridSize.x - 2, Grid.GridSize.y - 7));
		Grid.SpawnAgentAtLocation(Stone2Prefab, new Point2(Grid.GridSize.x - 2, Grid.GridSize.y - 5));
		Grid.SpawnAgentAtLocation(Stone2Prefab, new Point2(Grid.GridSize.x - 2, Grid.GridSize.y - 3));
		Grid.SpawnAgentAtLocation(Stone2Prefab, new Point2(Grid.GridSize.x - 2, Grid.GridSize.y - 1));
	}

	Player CreatePlayer(string name, Agent barrierPrefab, Point2 barrierLocation)
	{
		GameObject go = new GameObject(name);
		Player player = go.AddComponent<Player>();

		go.transform.SetParent(transform);
		m_Players.Add(player);

		player.Init(m_Players.Count, Grid.SpawnAgentAtLocation(barrierPrefab, barrierLocation));

		return player;
	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	int m_LocalPlayerIndex;
	List<Player> m_Players = new List<Player>();
	float m_PlacementTimer;

	static GameManager m_Instance;
}
