﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum HitResultFlags
{
	None = 0,
	Blocked_EdgeOfGrid	= (1 << 0),
	Blocked_HitAgent	= (1 << 1),
};

public class HitResult
{
	public bool HasFlag(HitResultFlags flag)
	{
		return (Flags & (int)flag) == (int)flag;
	}

	public List<Agent> HitAgents = new List<Agent>();
	public List<GridCell> OverlappingCells = new List<GridCell>();
	public int Flags = 0;

}

public class Grid : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC ENUMS                                                         */
	/************************************************************************/

	public enum PlacementStatusEnum
	{
		None,
		OffGrid,
		Blocked,
		ValidForPlayer,
		ValidForOpponent,
	};

	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/

	// Number of cells in the grid
	public Point2 GridSize;

	// Size (In world units) of each cell
	public Vector2 CellSize = Vector2.one;

	// Amount to inset each rendered cell
	public float CellInset = 0.1f;

	// Material to render cells with
	public Material CellMaterial;

	// Colors for the grid cells
	public Color ColorNormal = Color.grey;
	public Color ColorHighlight = Color.white;
	public Color ColorBlocked = Color.red;
	public Color ColorPlacement = Color.blue;

	/************************************************************************/
	/* PUBLIC PROPERTIES                                                    */
	/************************************************************************/

	public GridCell[] HighlightedCells { get { return m_HighlightedCells.ToArray(); } }

	public Agent[] Agents { get { return m_Agents.ToArray(); } }

	public PlacementStatusEnum PlacementStatus { get { return m_PlacementStatus; } }

	/************************************************************************/
	/* PUBLIC METHODS                                                       */
	/************************************************************************/

	public GridCell GetCell(Point2 location)
	{
		return GetCell(location.x, location.y);
	}

	public GridCell GetCell(int x, int y)
	{
		if (x < 0 || y < 0 || x >= GridSize.x || y >= GridSize.y)
		{
			return null;
		}
		else
		{
			return m_Cells[x, y];
		}
	}

	public bool IsCellHighlighted(GridCell cell)
	{
		return m_HighlightedCells != null && m_HighlightedCells.Contains(cell);
	}

	public Agent SpawnAgentAtPosition(Agent prefab, Vector2 position)
	{
		Player player;
		
		// HACK: Just setting the player to which side of the grid we clicked on
		if (position.x > ((GridSize.x / 2) * CellSize.x) - transform.position.x)
		{
			player = GameManager.Instance.OpponentPlayers[0];
		}
		else
		{
			player = GameManager.Instance.LocalPlayer;
		}

		Agent agent = GameObject.Instantiate(prefab);
		agent.Init(this, player, position);
		m_Agents.Add(agent);
		return agent;
	}

	public Agent SpawnAgentAtLocation(Agent agent, Point2 location)
	{
		return SpawnAgentAtPosition(agent, LocationToPosition(agent, location));
	}

	public void BeginPlacement(Agent agent)
	{
		m_PlacePrefab = agent;

		if (m_PlacePrefab)
		{
			m_PlacementStatus = PlacementStatusEnum.OffGrid;

		}
		else
		{
			m_PlacementStatus = PlacementStatusEnum.None;

		}
	}

	public PlacementStatusEnum CanPlaceAtPosition(Agent agent, Vector2 position)
	{
		if (agent == null)
		{
			return PlacementStatusEnum.None;
		}

		List<GridCell> cells = agent.GetCellsForFootprint(this, position);


		PlacementStatusEnum status = PlacementStatusEnum.OffGrid;

		foreach (GridCell cell in cells)
		{
			if (cell == null)
			{
				return PlacementStatusEnum.OffGrid;
			}

			if (!cell.CanPlace(agent))
			{
				return PlacementStatusEnum.Blocked;
			}

			if (cell.Location.x >= GridSize.x / 2)
			{
				if (PlacementStatus != PlacementStatusEnum.ValidForPlayer)
				{
					
					status = PlacementStatusEnum.ValidForOpponent;
				}
				else
				{
					return PlacementStatusEnum.OffGrid;
				}
			}
			else
			{
				if (PlacementStatus != PlacementStatusEnum.ValidForOpponent)
				{
					status = PlacementStatusEnum.ValidForPlayer;
				}
				else
				{
					return PlacementStatusEnum.OffGrid;
				}
			}
		}

		return status;
	}

	public PlacementStatusEnum CanPlaceAtLocation(Agent agent, Point2 location)
	{
		
		return CanPlaceAtPosition(agent, LocationToPosition(agent, location));
	}

	public bool HitTestAgent(Agent agent, Vector2 position, out HitResult hitResult)
	{
		hitResult = new HitResult();

		if (agent == null)
		{
			return false;
		}

		List<GridCell> cells = agent.GetCellsForFootprint(this, position);

		foreach (GridCell cell in cells)
		{
			if (cell == null)
			{
				hitResult.Flags |= (int)HitResultFlags.Blocked_EdgeOfGrid;
			}
			else
			{
				hitResult.OverlappingCells.Add(cell);

				if (cell.Occupant && cell.Occupant != agent)
				{
					hitResult.Flags |= (int)HitResultFlags.Blocked_HitAgent;
					hitResult.HitAgents.Add(cell.Occupant);
				}
			}
		}

		return hitResult.Flags != 0;
	}

	public int GetAgentCountForPlayer(Player player)
	{
		int count = 0;

		foreach (Agent agent in m_Agents)
		{
			if (agent.OwnerPlayer == player)
			{
				
				count++;
			}
		}

		return count;
	}

	/************************************************************************/
	/* MESSAGES                                                             */
	/************************************************************************/
	// Use this for initialization
	void Start () 
	{
		InitGrid();
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach (GridCell cell in m_Cells)
		{
			cell.SetOccupant(null);
		}

		List<Agent> destroyedAgents = new List<Agent>();

		foreach (Agent agent in m_Agents)
		{
			Unit unit = agent as Unit;

			if (unit && unit.CurrentHP <= 0)
			{
				agent.SendMessage("OnDie", SendMessageOptions.DontRequireReceiver);
				BroadcastMessage("AgentDestroyed", agent, SendMessageOptions.DontRequireReceiver);

				Destroy(agent.gameObject);
				destroyedAgents.Add(agent);
			}

			List<GridCell> occupiedCells = agent.GetOccupyingCells();

			foreach (GridCell cell in occupiedCells)
			{
				if (cell.Occupant)
				{
					Debug.LogWarning("Trying to double-occupy a cell!");
				}

				cell.SetOccupant(agent);
			}
		}

		foreach (Agent destroyedAgent in destroyedAgents)
		{
			m_Agents.Remove(destroyedAgent);
		}

		DoPlacement();
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;

		for (int x = 0; x <= GridSize.x; x++)
		{
			Vector3 from = transform.position + (x * CellSize.x) * transform.right;
			Vector3 to = from + transform.forward * (CellSize.y * GridSize.y);

			Gizmos.DrawLine(from, to);
		}

		for (int y = 0; y <= GridSize.y; y++)
		{
			Vector3 from = transform.position + (y * CellSize.y) * transform.forward;
			Vector3 to = from + transform.right * (CellSize.x * GridSize.x);

			Gizmos.DrawLine(from, to);
		}

	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	void InitGrid()
	{
		m_Cells = new GridCell[GridSize.x, GridSize.y];

		for (int y = 0; y < GridSize.y; y++)
		{
			for (int x = 0; x < GridSize.x; x++)
			{
				// Create all of the grid cells
				GameObject go = new GameObject("Cell" + x.ToString() + "," + y.ToString());
				GridCell gridCell = go.AddComponent<GridCell>();
				gridCell.Init(this, new Point2(x, y));
				m_Cells[x, y] = gridCell;
			}
		}
	}

	void DoPlacement()
	{
		Camera camera = Camera.main;

		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		Plane gridPlane = new Plane(Vector3.up, transform.position);
		Vector2 placementPoint = Vector2.zero;

		m_HighlightedCells = null;

		float distance;
		if (gridPlane.Raycast(ray, out distance))
		{
			Vector3 worldPoint = ray.GetPoint(distance);

			if (worldPoint.x >= 0.0f && worldPoint.x < GridSize.x * CellSize.x &&
				worldPoint.z >= 0.0f && worldPoint.z < GridSize.y * CellSize.y)
			{
				Vector2 postOffset = Vector2.zero;

				if (m_PlacePrefab)
				{
					// Offset the point depending on if the footprint is odd or even
					if (m_PlacePrefab.Footprint.x % 2 == 0)
					{
						worldPoint.x += CellSize.x * 0.5f;
					}
					else
					{
						postOffset.x = 0.5f;
					}

					if (m_PlacePrefab.Footprint.y % 2 == 0)
					{
						worldPoint.z += CellSize.y * 0.5f;
					}
					else
					{
						postOffset.y = 0.5f;
					}
				}

				// Align the placementPoint with the grid
				placementPoint = new Vector2((Mathf.Floor(worldPoint.x / CellSize.x) + postOffset.x) * CellSize.x,
											 (Mathf.Floor(worldPoint.z / CellSize.y) + postOffset.y) * CellSize.y);

				if (m_PlacePrefab)
				{
					m_HighlightedCells = m_PlacePrefab.GetCellsForFootprint(this, placementPoint);
				}
				else
				{
					Point2 location = new Point2(Mathf.FloorToInt(placementPoint.x), Mathf.FloorToInt(placementPoint.y));
					GridCell cell = GetCell(location);

					if (cell)
					{
						m_HighlightedCells = new List<GridCell>();
						m_HighlightedCells.Add(cell);
					}
					else
					{
						m_HighlightedCells = null;
					}
				}
			}
		}

		m_PlacementStatus = CanPlaceAtPosition(m_PlacePrefab, placementPoint);

		if (Input.GetMouseButtonDown(0) && m_PlacementStatus == PlacementStatusEnum.ValidForPlayer)
		{
			Agent agent = SpawnAgentAtPosition(m_PlacePrefab, placementPoint);

			foreach (GridCell cell in m_HighlightedCells)
			{
				cell.SetOccupant(agent);
			}

			m_PlacePrefab = null;
			m_HighlightedCells = null;
		}
	}

	Vector2 LocationToPosition(Agent agent, Point2 location)
	{
		Vector2 postOffset = Vector2.zero;
		Vector2 position = new Vector2(location.x * CellSize.x, location.y * CellSize.y);

		// Offset the point depending on if the footprint is odd or even
		if (agent.Footprint.x % 2 == 0)
		{
			position.x += CellSize.x * 0.5f;
		}
		else
		{
			postOffset.x = 0.5f;
		}

		if (agent.Footprint.y % 2 == 0)
		{
			position.y += CellSize.y * 0.5f;
		}
		else
		{
			postOffset.y = 0.5f;
		}

		// Align the position with the grid
		return new Vector2((Mathf.Floor(position.x / CellSize.x) + postOffset.x) * CellSize.x,
		                   (Mathf.Floor(position.y / CellSize.y) + postOffset.y) * CellSize.y);
	}

	/************************************************************************/
	/* PRIVATE VARIABLES                                                    */
	/************************************************************************/

	Agent m_PlacePrefab;
	List<GridCell> m_HighlightedCells;
	PlacementStatusEnum m_PlacementStatus = PlacementStatusEnum.None;
	GridCell[,] m_Cells;
	List<Agent> m_Agents = new List<Agent>();
}
