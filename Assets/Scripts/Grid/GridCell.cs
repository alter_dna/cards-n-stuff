﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridCell : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC PROPERTIES                                                    */
	/************************************************************************/

	public bool IsHighlighted { get { return m_ParentGrid.IsCellHighlighted(this); } }

	public Point2 Location { get { return m_Location; } }

	public Agent Occupant { get { return m_Occupant; } }

	public Vector2 LocalCenter
	{
		get
		{
			float x = (m_Location.x + 0.5f) * m_ParentGrid.CellSize.x;
			float y = (m_Location.y + 0.5f) * m_ParentGrid.CellSize.y;

			return (x * Vector2.right) + (y * Vector2.up);
		}
	}

	public Vector3 WorldCenter
	{
		get
		{
			return m_ParentGrid.transform.position + (m_ParentGrid.transform.right * LocalCenter.x) + (m_ParentGrid.transform.forward * LocalCenter.y);
		}
	}

	/************************************************************************/
	/* PUBLIC METHODS                                                       */
	/************************************************************************/

	public void Init(Grid parentGrid, Point2 location)
	{
		m_ParentGrid = parentGrid;
		m_Location = location;
		transform.SetParent(m_ParentGrid.transform);
	}

	public void SetOccupant(Agent occupant)
	{
		m_Occupant = occupant;
	}

	public bool CanPlace(Agent testAgent)
	{
		return m_Occupant == null || m_Occupant == testAgent;
	}

	/************************************************************************/
	/* MESSAGES                                                             */
	/************************************************************************/

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnRenderObject()
	{
		Color color = GetColor();
		m_ParentGrid.CellMaterial.SetPass(0);

		Vector3 right = m_ParentGrid.transform.right;
		Vector3 forward = m_ParentGrid.transform.forward;
		float offset = m_ParentGrid.CellInset;

		Vector2 cellSize = m_ParentGrid.CellSize;
		Vector2 position = new Vector2(m_Location.x * cellSize.x, m_Location.y * cellSize.y);
		Vector2[] points =
		{
			new Vector2(position.x + offset,				position.y + cellSize.y - offset),
			new Vector2(position.x + cellSize.x - offset,	position.y + cellSize.y - offset),
			new Vector2(position.x + cellSize.x - offset,	position.y + offset),
			new Vector2(position.x + offset,				position.y + offset)
		};

		Vector2[] UVs =
		{
			new Vector2(0.0f, 1.0f),
			new Vector2(1.0f, 1.0f),
			new Vector2(1.0f, 0.0f),
			new Vector2(0.0f, 0.0f),
		};

		GL.Begin(GL.QUADS);
		for (int i = 0; i < 4; i++)
		{
			GL.TexCoord(UVs[i]);
			GL.Color(color);
			GL.Vertex(m_ParentGrid.transform.position + (right * points[i].x) + (forward * points[i].y));
		}
		GL.End();
	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	Color GetColor()
	{
		if (m_ParentGrid.IsCellHighlighted(this))
		{
			if (m_ParentGrid.PlacementStatus == Grid.PlacementStatusEnum.None)
			{
				return m_ParentGrid.ColorHighlight;
			}
			else if (m_ParentGrid.PlacementStatus == Grid.PlacementStatusEnum.OffGrid || m_ParentGrid.PlacementStatus == Grid.PlacementStatusEnum.ValidForOpponent)
			{
				return m_ParentGrid.ColorBlocked;
			}
			else if (m_ParentGrid.PlacementStatus == Grid.PlacementStatusEnum.ValidForPlayer)
			{
				return m_Occupant ? m_ParentGrid.ColorBlocked : m_ParentGrid.ColorPlacement;
			}
		}
/*
		else if (m_ParentGrid.HighlightedCell)
		{
			Vector2 highlightedPosition = m_ParentGrid.HighlightedCell.LocalCenter;
			Vector2 ourPosition = LocalCenter;

			float distance = Vector2.Distance(highlightedPosition, ourPosition);
			float t = distance / 5.0f;

			if (t < 1.0f)
			{
				if (!m_Occupant)
				{
					return Color.Lerp(Color.blue, Color.grey, t);
				}
				else
				{
					return Color.red;
				}
			}
		}
*/

		return m_ParentGrid.ColorNormal;
	}

	/************************************************************************/
	/* PRIVATE VARIABLES                                                    */
	/************************************************************************/

	Agent m_Occupant;
	Grid m_ParentGrid;
	Point2 m_Location;
}
