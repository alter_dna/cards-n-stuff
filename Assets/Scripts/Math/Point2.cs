﻿using UnityEngine;

[System.Serializable]
[System.Diagnostics.DebuggerDisplay("x={x}, y={y}")]
public struct Point2
{
    public int x;
    public int y;

    public static Point2 zero = new Point2(0, 0);
    public static Point2 one = new Point2(1, 1);
    public static Point2 left = new Point2(-1, 0);
    public static Point2 right = new Point2(1, 0);
    public static Point2 up = new Point2(0, -1);
    public static Point2 down = new Point2(0, 1);

    public Point2(int _x, int _y)
    {
        x = _x;
        y = _y;
    }

    public static Point2 FromVector2(Vector2 v)
    {
        return new Point2((int)v.x, (int)v.y);
    }

    public Vector2 ToVector2()
    {
        return new Vector2(x, y);
    }

    public Vector3 ToVector3XY()
    {
        return new Vector3(x, y, 0);
    }

	public Vector3 ToVector3XZ()
	{
		return new Vector3(x, y, 0);
	}

    public static Point2 operator +(Point2 a, Point2 b)
    {
        return new Point2(a.x + b.x, a.y + b.y);
    }

    public static Point2 operator -(Point2 a, Point2 b)
    {
        return new Point2(a.x - b.x, a.y - b.y);
    }

    public static bool operator ==(Point2 a, Point2 b)
    {
        return a.x == b.x && a.y == b.y;
    }

    public static bool operator !=(Point2 a, Point2 b)
    {
        return a.x != b.x || a.y != b.y;
    }

    public override bool Equals(object obj)
    {
        Point2? b = obj as Point2?;
        if(b.HasValue)
        {
            return x == b.Value.x && y == b.Value.y;
        }

        return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }



	public override string ToString()
	{
		return x.ToString() + ", " + y.ToString();
	}
}
