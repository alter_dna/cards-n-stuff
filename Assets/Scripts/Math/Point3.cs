﻿using UnityEngine;

[System.Serializable]
[System.Diagnostics.DebuggerDisplay("x={x}, y={y}, z={z}")]
public struct Point3
{
    public int x;
    public int y;
	public int z;

    public static Point3 zero		= new Point3( 0,  0,  0);
    public static Point3 one		= new Point3( 1,  1,  1);
	public static Point3 right		= new Point3( 1,  0,  0);
	public static Point3 up			= new Point3( 0,  1,  0);
	public static Point3 forward	= new Point3( 0,  0,  1);
	public static Point3 left		= new Point3(-1,  0,  0);
    public static Point3 down		= new Point3( 0, -1,  0);
	public static Point3 back		= new Point3( 0,  0, -1);

    public Point3(int _x, int _y, int _z)
    {
        x = _x;
        y = _y;
		z = _z;
    }

    public static Point3 FromVector3(Vector3 v)
    {
        return new Point3((int)v.x, (int)v.y, (int)v.z);
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }

    public static Point3 operator +(Point3 a, Point3 b)
    {
        return new Point3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static Point3 operator -(Point3 a, Point3 b)
    {
        return new Point3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static bool operator ==(Point3 a, Point3 b)
    {
        return a.x == b.x && a.y == b.y && a.z == b.z;
    }

    public static bool operator !=(Point3 a, Point3 b)
    {
        return a.x != b.x || a.y != b.y || a.z != b.z;
    }

    public override bool Equals(object obj)
    {
        Point3? b = obj as Point3?;
        if(b.HasValue)
        {
            return x == b.Value.x && y == b.Value.y && z == b.Value.z;
        }

        return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

	public override string ToString()
	{
		return x.ToString() + ", " + y.ToString() + ", " + z.ToString();
	}

	public static Point3 CreateRandom(Point3 min, Point3 max)
	{
		return new Point3(	
							Random.Range(min.x, max.x + 1),
							Random.Range(min.y, max.y + 1),
							Random.Range(min.z, max.z + 1)
						 );
	}
}
