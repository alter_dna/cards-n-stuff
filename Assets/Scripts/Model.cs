﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
#endif
using System.Collections.Generic;

[ExecuteInEditMode]
public class Model : MonoBehaviour
{
    public GameObject Prefab;
    public bool InheritLayer = true;

    [System.Serializable]
    public class MaterialReplaceData
    {
        public Material Find;
        public Material Replace;
    }

    public List<MaterialReplaceData> MaterialReplacements = new List<MaterialReplaceData>();

    [HideInInspector]
    public GameObject Instance;

    public Transform FindNode(string name, Transform source = null)
    {
        if (source == null)
        {
            source = Instance.transform;
        }

		if (source.name == name)
		{
			return source;
		}
		
		foreach (Transform transform in source)
		{
			Transform foundTransform = FindNode(name, transform);
			
			if (foundTransform)
			{
				return foundTransform;
			}
		}
		
        if (source == null)
        {
            Debug.LogError("FindNode: Couldn't find node: " + name);
        }
		
		return null;
    }

	public Material[] FindMaterialInstances(Material find)
	{
		List<Material> results = new List<Material>();

        Renderer[] renderers = Instance.GetComponentsInChildren<Renderer>(true);
		foreach (Renderer renderer in renderers)
		{
			if (renderer.sharedMaterial == find)
			{
				results.Add(renderer.material);
			}
		}

		return results.ToArray();
	}

    public void ReplaceMaterial(Material find, Material replace)
    {
        Renderer[] renderers = Instance.GetComponentsInChildren<Renderer>(true);
        foreach (Renderer renderer in renderers)
        {
            if (renderer.material == find || !find)
            {
                renderer.material = replace;
            }
        }
    }

    void Update()
    {
#if UNITY_EDITOR
        EditorUpdate();
#endif
       
        if (m_FinishedCallback != null && !IsAnimating())
        {
            OnAnimFinished oldCallback = m_FinishedCallback;
            m_CurrentAnimName = null;
            m_FinishedCallback = null;

            oldCallback();
        }

        if (Instance && m_FadeTime < m_TargetFadeTime)
        {
            m_FadeTime += Time.deltaTime;

            if (m_FadeTime < m_TargetFadeTime)
            {
                m_Color = Color.Lerp(m_StartColor, m_TargetColor, m_FadeTime / m_TargetFadeTime);
            }
            else
            {
                m_Color = m_TargetColor;
            }

            foreach (Renderer mr in Instance.GetComponentsInChildren(typeof(Renderer), true))
            {
                foreach (Material mat in mr.materials)
                {
                    mat.color = m_Color;
                }
            }
        }
    }

#if UNITY_EDITOR	
	// Struct of all components. Used for edit-time visualization and gizmo drawing
	public struct MeshContainer
    {
		public Mesh mesh;
		public Matrix4x4 matrix;
		public List<Material> materials;
	}

	[System.NonSerializedAttribute]
    public List<MeshContainer> MeshContainers = new List<MeshContainer>();

	void OnValidate() 
    {
		MeshContainers.Clear();

        if (enabled)
        {
            Rebuild(Prefab, Matrix4x4.identity);
        }
	}

	void OnEnable () 
    {
        MeshContainers.Clear();

		if (enabled)
        {
            Rebuild(Prefab, Matrix4x4.identity);
        }
	}

	void Rebuild (GameObject source, Matrix4x4 inMatrix) 
    {
		if (!source)
        {
            return;
        }

		Matrix4x4 baseMat = inMatrix * Matrix4x4.TRS(-source.transform.position, Quaternion.identity, Vector3.one);

        foreach (Renderer mr in source.GetComponentsInChildren(typeof(MeshRenderer), true))
		{
            if (mr.GetComponent<MeshFilter>())
            {
                MeshContainers.Add(new MeshContainer()
                {
                    mesh = mr.GetComponent<MeshFilter>().sharedMesh,
                    matrix = baseMat * mr.transform.localToWorldMatrix,
                    materials = new List<Material>(mr.sharedMaterials)
                });
            }
		}

        foreach (SkinnedMeshRenderer smr in source.GetComponentsInChildren(typeof(SkinnedMeshRenderer), true))
        {
            MeshContainers.Add(new MeshContainer()
                {
                    mesh = smr.sharedMesh,
                    matrix = baseMat * smr.transform.localToWorldMatrix,
                    materials = new List<Material>(smr.sharedMaterials)
                });
        }

		foreach (Model model in source.GetComponentsInChildren(typeof(Model), true))
		{
			if (model.enabled && model.gameObject.activeSelf)
            {
                Rebuild(model.Prefab, baseMat * model.transform.localToWorldMatrix);
            }
		}		
	}

	// Editor-time-only update: Draw the meshes so we can see the objects in the scene view
	void EditorUpdate () 
    {
		if (EditorApplication.isPlaying)
        {
            return;
        }

		Matrix4x4 mat = transform.localToWorldMatrix;
		foreach (MeshContainer mesh in MeshContainers)
        {
            for (int i = 0; i < mesh.materials.Count; i++)
            {
                Graphics.DrawMesh(mesh.mesh, mat * mesh.matrix, mesh.materials[i], gameObject.layer, null, i);
            }
        }
	}

	// Picking logic: Since we don't have gizmos.drawmesh, draw a bounding cube around each MeshContainer
	void OnDrawGizmos() 
    { 
        DrawGizmos(new Color(0, 0, 0, 0)); 
    }

	void OnDrawGizmosSelected() 
    { 
        DrawGizmos(new Color(0, 0, 1, 0.2f)); 
    }

	void DrawGizmos(Color col) 
    {
		if (EditorApplication.isPlaying)
        {
            return;
        }

		Matrix4x4 mat = transform.localToWorldMatrix;

        foreach (MeshContainer mesh in MeshContainers)
		{
			Gizmos.matrix = mat * mesh.matrix;

			if (mesh.mesh)
			{
				Gizmos.color = Color.clear;
				Gizmos.DrawCube(mesh.mesh.bounds.center, mesh.mesh.bounds.size);

				Gizmos.color = col;
				Gizmos.DrawWireCube(mesh.mesh.bounds.center, mesh.mesh.bounds.size);
			}
        }		
	}

/*
	// Baking stuff: Copy in all the referenced objects into the scene on play or build
	[PostProcessScene(-2)]
	public static void OnPostprocessScene() 
    {
        foreach (Model model in UnityEngine.Object.FindObjectsOfType(typeof(Model)))
        {
            BakeInstance(model);
        }
	}
*/
#endif

    void Awake()
    {
#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
#endif
        {
            BakeInstance(this);
        }
    }

    public static void BakeInstance(Model model)
    {
        if (!model.Prefab || model.Instance)
        {
            return;
        }

#if UNITY_EDITOR
        GameObject go = PrefabUtility.InstantiatePrefab(model.Prefab) as GameObject;
#else
        GameObject go = Instantiate(model.Prefab) as GameObject;
#endif
        Quaternion rot = go.transform.localRotation;
        Vector3 scale = go.transform.localScale;
        go.transform.parent = model.transform;
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale = scale;
        go.transform.localRotation = rot;
        model.Prefab = null;
        model.Instance = go;
		model.m_Animation = go.GetComponent<Animation>();
		model.m_Renderer = go.GetComponent<Renderer>();

        if (model.InheritLayer)
        {
            go.SetLayerRecursively(model.gameObject.layer);
        }

        foreach (Model child in go.GetComponentsInChildren<Model>())
        {
            BakeInstance(child);
        }

        foreach (MaterialReplaceData materialReplace in model.MaterialReplacements)
        {
            model.ReplaceMaterial(materialReplace.Find, materialReplace.Replace);
        }

#if UNITY_EDITOR
		if (EditorApplication.isPlaying)
#endif
        {
            model.SendMessage("OnModelInstantiated", SendMessageOptions.DontRequireReceiver);
        }
    }
    
    public delegate void OnAnimFinished();

    // Helper functions
    public bool IsAnimating()
    {
        return m_Animation && m_Animation.isPlaying && m_Animation[m_CurrentAnimName].enabled;
    }

	public bool HasAnimation(string name)
	{
		return m_Animation && m_Animation.GetClip(name) != null;
	}

	public void BlendToAnimation(string from, string to, float blendTime, OnAnimFinished callback = null)
	{
		if (m_Animation)
		{
			if (m_FinishedCallback != null)
			{
				m_FinishedCallback();
			}

			if (m_Animation[from])
			{
				m_Animation[from].weight = 1.0f;
				m_Animation.Blend(from, 0.0f, blendTime);
			}
			
			m_CurrentAnimName = to;
			
			m_Animation[to].weight = 0.0f;
			m_Animation.Blend(to, 1.0f, blendTime);
			m_FinishedCallback = callback;
		}
	}

    public void PlayAnimation(string name, OnAnimFinished callback = null)
    {
        if (m_Animation)
        {
            if (m_FinishedCallback != null)
            {
                m_FinishedCallback();
            }

            m_CurrentAnimName = name;
			m_Animation.Play(name);
            m_FinishedCallback = callback;
        }
    }

    public Vector3 GetCenter()
    {
		if (m_Renderer)
		{
			return m_Renderer.bounds.center;
		}
		else
		{
			return transform.position;
		}
    }

    public void FadeColorTo(Color color, float time)
    {
        if (time > 0.0f)
        {
            m_StartColor = m_Color;
            m_TargetColor = color;
            m_FadeTime = 0.0f;
            m_TargetFadeTime = time;
        }
        else
        {
            m_Color = m_TargetColor;
        }
    }

	public void OverrideMaterial(Material material, bool useOldTextures)
	{
		if (material != null)
		{
			if (m_OldMaterials == null)
			{
				m_OldMaterials = new List<OldMaterial>();

				Renderer[] renderers = Instance.GetComponentsInChildren<Renderer>(true);
				foreach (Renderer renderer in renderers)
				{
					m_OldMaterials.Add(new OldMaterial() { Renderer = renderer, Material = renderer.material });
				}
			}

			foreach (OldMaterial oldMaterial in m_OldMaterials)
			{
				if (useOldTextures)
				{
					oldMaterial.Renderer.material = new Material(material);
					oldMaterial.Renderer.material.SetTexture(0, oldMaterial.Material.GetTexture(0));
				}
				else
				{
					oldMaterial.Renderer.material = material;
				}
			}
		}
		else if (m_OldMaterials != null)
		{
			foreach (OldMaterial oldMaterial in m_OldMaterials)
			{
				oldMaterial.Renderer.material = oldMaterial.Material;
			}

			m_OldMaterials = null;
		}
	}

	public void SetOverrideMaterialValue(string name, object value)
	{
		if (m_OldMaterials != null)
		{
			foreach (OldMaterial oldMaterial in m_OldMaterials)
			{
				if (value.GetType() == typeof(int))
				{
					oldMaterial.Renderer.material.SetInt(name, (int)value);
				}
				else if (value.GetType() == typeof(float))
				{
					oldMaterial.Renderer.material.SetFloat(name, (float)value);
				}
			}
		}
	}

	public Renderer FindRenderer(string path)
	{
		Transform node = FindNode(path);

		if (node)
		{
			return node.GetComponent<Renderer>();
		}

		return null;
	}

	class OldMaterial
	{
		public Renderer Renderer;
		public Material Material;
	}

	List<OldMaterial> m_OldMaterials;
    string m_CurrentAnimName;
    OnAnimFinished m_FinishedCallback = null;
	Animation m_Animation;
	Renderer m_Renderer;

    Color m_Color = Color.white;
    Color m_StartColor;
    Color m_TargetColor;
    float m_FadeTime;
    float m_TargetFadeTime;
}
