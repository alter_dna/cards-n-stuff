﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/

	public int ManaMax;

	/************************************************************************/
	/* PUBLIC PROPERTIES                                                    */
	/************************************************************************/

	public int Mana { get { return m_Mana; } }
	public Agent Barrier { get { return m_Barrier; } }

	/************************************************************************/
	/* PUBLIC METHODS                                                       */
	/************************************************************************/

	public void Init(int playerIndex, Agent barrier)
	{
		m_PlayerIndex = playerIndex;
		m_Barrier = barrier;
	}

	public bool TrySpendMana(int spend)
	{
		if (spend > m_Mana)
		{
			m_Mana -= spend;
			return true;
		}
		else
		{
			return false;
		}
	}

	public int GetModifiedStatForUnit(Unit unit, Unit.UnitStat stat)
	{
		int value = unit.GetBaseStat(stat);

		foreach (StatModifier modifier in m_ActiveModifiers)
		{
			modifier.ApplyForUnit(unit, stat, ref value);
		}

		return value;
	}

	public void AddModifier(StatModifier modifier)
	{
		if (!m_ActiveModifiers.Contains(modifier))
		{
			m_ActiveModifiers.Add(modifier);
		}
		else
		{
			Debug.LogWarning("Tried to add a modifier that was already applied: " + modifier.name);
		}
	}

	public void RemoveModifier(StatModifier modifier)
	{
		if (m_ActiveModifiers.Contains(modifier))
		{
			m_ActiveModifiers.Remove(modifier);
		}
		else
		{
			Debug.LogWarning("Tried to remove a modifier that wasn't applied: " + modifier.name);
		}
	}

	public Quaternion GetSpawnRotationForAgent(Agent agent)
	{
		// TODO: Should be a better way of getting the rotation for a spawned agent
		if (GameManager.Instance.LocalPlayer == this)
		{
			return Quaternion.Euler(0, 90, 0);
		}
		else
		{
			return Quaternion.Euler(0, -90, 0);
		}
	}

	/************************************************************************/
	/* PRIVATE VARIABLES                                                    */
	/************************************************************************/

	int m_Mana;
	int m_PlayerIndex = -1;
	Agent m_Barrier;
	List<StatModifier> m_ActiveModifiers = new List<StatModifier>();
}
