﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerStats : MonoBehaviour {

	public Unit unit;
	public Unit enemyunit;
	public float Health;
	public float EnemyHealth;

	public float manaRegen;

	public Image healthBar;
	public Image manaBar;
	public Image enemyHealthBar;

	public Sprite[] ManaSprites;
	public Image ManaUI;
	public int Mana;
	//public GameObject gameOverPanel;
	public Button buttons;
	public Button buttons2;
	public Button buttons3;
	public Button buttons4;
	public Button buttons5;

	public Text Mtext;
	public Text Ptext;
	public Text Etext;

	public int StonesCount;

	void Start () 
	{
		unit = GameObject.FindWithTag("Barrier").GetComponent<Unit>();
		enemyunit = GameObject.FindWithTag("EnemyBarrier").GetComponent<Unit>();
		StartCoroutine (ManaRegen());
		//gameOverPanel.SetActive (false);
		//unit.BaseAttack = 5;
		//enemyunit.BaseAttack = 5;
	}
	
	// Update is called once per frame
	void Update () {

		if (Mana >= 100) {
			Mana = 100;
		}

		Health = unit.CurrentHP;
		EnemyHealth = enemyunit.CurrentHP;
		healthBar.rectTransform.localScale = new Vector3 (Health / 100, healthBar.rectTransform.localScale.y, healthBar.rectTransform.localScale.z);
		manaBar.rectTransform.localScale = new Vector3 (Mana, manaBar.rectTransform.localScale.y, manaBar.rectTransform.localScale.z);
		enemyHealthBar.rectTransform.localScale = new Vector3 (EnemyHealth / 100, enemyHealthBar.rectTransform.localScale.y, enemyHealthBar.rectTransform.localScale.z);

		Ptext.text = "" + unit.CurrentHP + " / " + "200" ;
		Etext.text = "" + enemyunit.CurrentHP + " / " + "200" ;
		Mtext.text = "" + Mana + " / " + "100" ;

		//ManaUI.sprite = ManaSprites [Mana];

//		if (Mana < 1) {
//			buttons.interactable = false;
//			buttons2.interactable = false;
//			buttons3.interactable = false;
//			buttons4.interactable = false;
//			buttons5.interactable = false;
//			Mana = 0;
//		} else {
//			buttons.interactable = true;
//			buttons2.interactable = true;
//			buttons3.interactable = true;
//			buttons4.interactable = true;
//			buttons5.interactable = true;
//		}




//		if (Health <= 0) 
//		{
//			//gameOverPanel.SetActive (true);
//		}
//			
//		if (unit.CurrentHP <= 150) 
//		{
//			unit.BaseAttack = 2;
//		}
//
//		if (unit.CurrentHP <= 100) 
//		{
//			unit.BaseAttack = 3;
//		}
//
//		if (unit.CurrentHP <= 50) 
//		{
//			unit.BaseAttack = 4;
//		}
//
//		EnemyHealth = enemyunit.CurrentHP;
//
//
//		if (enemyunit.CurrentHP <= 150) 
//		{
//			enemyunit.BaseAttack = 2;
//		}
//
//		if (enemyunit.CurrentHP <= 100) 
//		{
//			enemyunit.BaseAttack = 3;
//		}
//
//		if (enemyunit.CurrentHP <= 50) 
//		{
//			enemyunit.BaseAttack = 4;
//		}

	}

	IEnumerator ManaRegen()
	{
		while (true) {
			ManaRestore ();
			yield return new WaitForSeconds (manaRegen);
		}

	}

	public void ManaRestore()
	{
		Mana++;
	}

	public void GameOverPanel(bool c)
	{
		if (c) {
//			gameOverPanel.SetActive (true);
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
			
	}

	public void Restart()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

	void OnGUI()
	{
		GUI.Label(new Rect (20,300,200,20), "Player Health: " + Health.ToString());
	//	GUI.Label(new Rect (20,20,200,20), "Health: " + unit.CurrentHP.ToString());
		GUI.Label(new Rect (20,320,200,20), "ATK: " + unit.BaseAttack.ToString());

		GUI.Label(new Rect (Screen.width - 200,300,200,20), "Enemy Health: " + EnemyHealth.ToString());
		//	GUI.Label(new Rect (20,20,200,20), "Health: " + unit.CurrentHP.ToString());
		GUI.Label(new Rect (Screen.width - 200,320,200,20), "ATK: " + enemyunit.BaseAttack.ToString());

	}
}
