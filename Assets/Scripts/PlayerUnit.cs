﻿using UnityEngine;
using System.Collections;

public class PlayerUnit : MonoBehaviour {

	private PlayerStats stats;
	public int cost;

	// Use this for initialization
	void Start () {
		stats = GameObject.FindWithTag("Manager").GetComponent<PlayerStats>();
		stats.Mana -= cost;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}
