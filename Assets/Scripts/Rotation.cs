﻿using UnityEngine;

public class Rotation : MonoBehaviour 
{

	public float rotSpeedX;
	public float rotSpeedY;
	public float rotSpeedZ;

	void Start()
	{
		//rotSpeed = 10;
	}

	void Update() 
	{
		// Rotate the object around its local Y axis at 1 degree per second
		transform.Rotate(Time.deltaTime * rotSpeedX, Time.deltaTime * rotSpeedY, Time.deltaTime * rotSpeedZ);

		// ...also rotate around the World's Y axis
		//transform.Rotate(Vector3.up * Time.deltaTime, Space.World);
	}
}