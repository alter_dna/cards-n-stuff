﻿using UnityEngine;

[RequireComponent(typeof(Light))]
public class SoftFlicker : MonoBehaviour
{
	public float minIntensity;
	public float maxIntensity;
	public float flickerSpeed;

	float random;

	void Start()
	{
		//flickerSpeed = 0f;
		//this.GetComponent<Light>().intensity = 0;
	//random = Random.Range(0.0f, 65535.0f);
	}

	void Update()
	{
		float noise = Mathf.PerlinNoise(random, Time.time * flickerSpeed);
		GetComponent<Light>().intensity = Mathf.Lerp(minIntensity, maxIntensity, noise);
	}
}