﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatModifier : MonoBehaviour 
{
	/************************************************************************/
	/* PUBLIC ENUMS                                                         */
	/************************************************************************/

	public enum ModifierType
	{
		Additive,
		Multiplicative
	};

	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/

	public Unit.UnitStat Stat = Unit.UnitStat.Attack;
	public ModifierType Type = ModifierType.Additive;
	public int Value = 0;

	[EnumFlag]
	public Unit.UnitType UnitTypeFilter = (Unit.UnitType)EnumUtils.GetAllFlagsSet<Unit.UnitType>();
	[EnumFlag]
	public Unit.SpeciesType SpeciesFilter = (Unit.SpeciesType)EnumUtils.GetAllFlagsSet<Unit.SpeciesType>();

	/************************************************************************/
	/* PUBLIC METHODS                                                       */
	/************************************************************************/

	public bool ApplyForUnit(Unit unit, Unit.UnitStat stat, ref int value)
	{
		if (stat != Stat)
		{
			return false;
		}

		if (((int)UnitTypeFilter & (int)unit.Type) == 0)
		{
			return false;
		}

		if (((int)SpeciesFilter & (int)unit.Species) == 0)
		{
			return false;
		}

		if (Type == ModifierType.Additive)
		{
			value += Value;
		}
		else if (Type == ModifierType.Multiplicative)
		{
			int baseValue = unit.GetBaseStat(Stat);
			value += (baseValue * Value) - baseValue;
		}
		else
		{
			Debug.LogError("Invalid modifier type!");
			return false;
		}

		return true;
	}
}
