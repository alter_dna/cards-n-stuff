﻿using UnityEngine;
using System.Collections;

class StoneEnemy : Unit
{
	public int DamageToCause = 5;

	public Unit stat;
	
	void OnDie()
	{
		stat = GameObject.FindWithTag("EnemyBarrier").GetComponent<Unit>();
		stat.BaseAttack -= 1;
		OwnerPlayer.Barrier.TakeDamage(null, DamageToCause);
	}
}