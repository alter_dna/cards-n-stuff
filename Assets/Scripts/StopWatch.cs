﻿using UnityEngine;
using System.Collections;

public class StopWatch : MonoBehaviour {

	//public float timeToStart=0.0f;
	//public float timeToEnd=30.0f;
	public float timeRemaining;

	void Update()
	{
		timeRemaining += 1 * Time.deltaTime;
	}
	
	void OnGUI() {
		//float timeRemaining=timeToEnd - (Time.time + timeToStart);
		GUI.Label(new Rect(Screen.width/2, 10, 100, 100), "Time left: " + timeRemaining);
	}
}
