﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIButton : MonoBehaviour {

	public int unitNum;
	public int unitNumPlus;
	public int cardCost;

	public GameManager gm;
	private PlayerStats stats;
	public Text text;

	public Button myButton;

	// Use this for initialization
	void Start () {
		gm = GameObject.FindWithTag("Manager").GetComponent<GameManager>();
		stats = GameObject.FindWithTag("Manager").GetComponent<PlayerStats>();
		//GameManager.unitNumber ();
		//text = GetComponent <GameManager>();
		//unitNum = gm.unitNumber+unitNumPlus;
		//gm.unitNumber = unitNum;
		//text.text = "Soldier";
	}

	void Update()
	{
		//unitNum = gm.unitNumber+unitNumPlus;
		if (stats.Mana < cardCost) {
			myButton.interactable = false;
		}

		if (stats.Mana >= cardCost) {
			myButton.interactable = true;
		}
	}

}
