﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Unit : Agent
{
	/************************************************************************/
	/* PUBLIC ENUMS                                                         */
	/************************************************************************/


	[Flags]
	public enum UnitType
	{
		Offense	= (1 << 0),
		Defense	= (1 << 1),
		Magic	= (1 << 2),
	};

	[Flags]
	public enum SpeciesType
	{
		Beast		= (1 << 0),
		Demon		= (1 << 1),
		Undead		= (1 << 2),
		Dragon		= (1 << 3),
		Nymph		= (1 << 4),
		Elemental	= (1 << 5),
		Eaonan		= (1 << 6),
		Estonan		= (1 << 7),
		Mherania	= (1 << 8),
		Apathan		= (1 << 9),
		SkaAra		= (1 << 10),
		Fae			= (1 << 11),
		Adarhalim	= (1 << 12),
		Mech		= (1 << 13),
		Structure	= (1 << 14),
	};

	public enum UnitStat
	{
		HP,
		Attack,
		Cost
	};

	/************************************************************************/
	/* PUBLIC VARIABLES                                                     */
	/************************************************************************/
	
	[Header("Unit Settings")]
	public UnitType Type;
	public SpeciesType Species;

	public int BaseHP;
	public int BaseAttack;
	public int BaseCost;

	/************************************************************************/
	/* PUBLIC PROPERTIES                                                    */
	/************************************************************************/

	public int CurrentHP { get { return GetStat(UnitStat.HP) - m_Damage; } }
	//public int CurrentATK { get { return GetStat(UnitStat.Attack) - m_Damage; } }

	/************************************************************************/
	/* PUBLIC METHODS                                                       */
	/************************************************************************/

	public int GetBaseStat(UnitStat stat)
	{
		switch (stat)
		{
			case UnitStat.HP:
				return BaseHP;

			case UnitStat.Attack:
				return BaseAttack;

			case UnitStat.Cost:
				return BaseCost;

			default:
				Debug.LogError("Invalid stat type: " + stat.ToString());
				return 0;
		}
	}

	public int GetStat(UnitStat stat)
	{
		return GetStat(OwnerPlayer, stat);
	}

	public int GetStat(Player player, UnitStat stat)
	{
		return player.GetModifiedStatForUnit(this, stat);
	}

	public override void TakeDamage(Agent attacker, int damage)
	{
		base.TakeDamage(attacker, damage);

		m_Damage += damage;
	}

	/************************************************************************/
	/* MESSAGES                                                             */
	/************************************************************************/

	// Use this for initialization
	protected override void Start() 
	{
		base.Start();

		m_Model = GetComponent<Model>();
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		base.Update();

		UpdateActions();
		UpdateAnimations();
	}

	protected virtual void OnGUI()
	{
		Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
		Rect screenRect = new Rect(screenPos.x, Screen.height - screenPos.y, 100, 100);
		Rect screenATKRect = new Rect(screenPos.x, Screen.height - screenPos.y-20, 100, 100);

		GUI.Label(screenRect, "HP: " + CurrentHP.ToString() + "/" + GetStat(UnitStat.HP).ToString());
		GUI.Label(screenATKRect, "ATK: " + BaseAttack.ToString() + "/" + GetStat(UnitStat.Attack).ToString());
    }

	protected virtual void OnAnimationEvent(AnimEventMessage msg)
	{
		if (msg.EventName == "DoAttack")
		{
			DoAttack();
		}
	}

	protected virtual void AgentDestroyed(Agent agent)
	{
		if (agent == this)
		{
			return;
		}

		if (m_AttackTargets.Contains(agent))
		{
			m_AttackTargets.Remove(agent);
		}
	}

	/************************************************************************/
	/* PRIVATE METHODS                                                      */
	/************************************************************************/

	void UpdateActions()
	{
		if (m_AttackTargets.Count > 0)
		{
			return;
		}

		bool canMove = (Speed != 0.0f && m_Model.HasAnimation("walk"));

		if (!IsMoving)
		{
			if (canMove)
			{
				Point2 offset = Point2.zero;
				offset.x = (transform.forward.x > 0) ? 1 : -1;

				HitResult hitResult = new HitResult();
				if (!MoveCellOffset(offset, out hitResult))
				{
					if (hitResult.HasFlag(HitResultFlags.Blocked_HitAgent))
					{
						m_AttackTargets = FindValidOpponentAgents(hitResult.HitAgents);
					}

					if (m_AttackTargets.Count > 0)
					{
						PlayAttackAnim();
					}
					else
					{
						m_NextAnim = "idle";
					}
				}
				else
				{
					m_NextAnim = "walk";
				}
			}
			else
			{
				Point2 offset = Point2.zero;
				offset.x = (transform.forward.x > 0) ? 1 : -1;
				Vector3 targetPosition = transform.position + new Vector3(offset.x * ParentGrid.CellSize.x, 0.0f, offset.y * ParentGrid.CellSize.y);

				HitResult hitResult;
				if (ParentGrid.HitTestAgent(this, new Vector2(targetPosition.x, targetPosition.z), out hitResult))
				{
					if (hitResult.HasFlag(HitResultFlags.Blocked_HitAgent))
					{
						m_AttackTargets = FindValidOpponentAgents(hitResult.HitAgents);
					}
				}

				if (m_AttackTargets.Count > 0)
				{
					PlayAttackAnim();
				}
				else
				{
					m_NextAnim = "idle";
				}
			}
		}
		else
		{
			m_NextAnim = (IsMoving && canMove) ? "walk" : "idle";
		}
	}

	void UpdateAnimations()
	{
		if (m_Model)
		{
			if (m_NextAnim != m_CurrentAnim)
			{
				if (m_Model.HasAnimation(m_NextAnim))
				{
					if (m_CurrentAnim == null)
					{
						m_Model.PlayAnimation(m_NextAnim, m_NextAnimCallback);
					}
					else
					{
						m_Model.BlendToAnimation(m_CurrentAnim, m_NextAnim, 0.25f, m_NextAnimCallback);
					}
				}
				else if (m_NextAnimCallback != null)
				{
					m_NextAnimCallback();
				}

				m_CurrentAnim = m_NextAnim;
				m_NextAnimCallback = null;
			}
		}
	}

	void PlayAttackAnim()
	{
		m_NextAnim = "attack";
		m_NextAnimCallback = OnAttackFinished;

		if (m_NextAnim == m_CurrentAnim)
		{
			m_CurrentAnim = null;
		}
	}

	protected virtual void DoAttack()
	{
		int damage = GetStat(UnitStat.Attack);

		foreach (Agent agent in m_AttackTargets)
		{
			agent.TakeDamage(this, damage);
		}
	}

	void OnAttackFinished()
	{
		m_AttackTargets.Clear();
	}

	List<Agent> FindAllOpponentAgents(List<Agent> listToSearch)
	{
		List<Agent> result = new List<Agent>();

		foreach (Agent agent in listToSearch)
		{
			if (agent.OwnerPlayer != OwnerPlayer)
			{
				result.Add(agent);
			}
		}

		return result;
	}

	List<Agent> FindValidOpponentAgents(List<Agent> listToSearch)
	{
		List<Agent> result = new List<Agent>();

		foreach (Agent agent in listToSearch)
		{
			if (agent.OwnerPlayer != OwnerPlayer && !agent.IsMoving)
			{
				result.Add(agent);
			}
		}

		return result;
	}

	/************************************************************************/
	/* PRIVATE VARIABLES                                                    */
	/************************************************************************/

	// Amount of damage this unit has taken
	int m_Damage = 0;

	Model m_Model;
	Model.OnAnimFinished m_NextAnimCallback = null;
	string m_NextAnim = "idle";
	string m_CurrentAnim = null;
	List<Agent> m_AttackTargets = new List<Agent>();
}
