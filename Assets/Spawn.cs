﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
    // The Ship
	public GameObject[] spawnPoints;
    public GameObject[] ships;
	public Vector3 position;

    // The Interval
    public float interval = 1;

    // Use this for initialization
    void Start () {
		//SpawnNext();
		InvokeRepeating("SpawnNext",0, interval);

		//Vector3 position = new Vector3(Random.Range(7.5f,7.5f), Random.Range(-3.5f,3.5f), 0);
    }

	void Update()
		{
		
		}

    void SpawnNext () {
		int spawn = Random.Range (0, spawnPoints.Length);
		int ship = Random.Range (0, ships.Length);
		//float randomTime = Random.Range (minSpawnTime, maxSpawnTime);
		GameObject.Instantiate(ships[ship], spawnPoints[spawn].transform.position, transform.rotation);
//		Vector3 scale = ship.transform.localScale;
//		scale.x = Random.Range (0.25f, 1);
//		scale.y = scale.x;
//		ship.transform.localScale = scale;
	//	Instantiate(ship, position, Quaternion.identity);
    }
}
